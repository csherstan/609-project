#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <termios.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <float.h>
#include <signal.h>
#include "csp3.h"
#include "normal.h"
#include <time.h>
#include "math.h"

double norm(double * arr, int length) {
	double sum = 0.0;
	int i;
	for (i = 0; i < length; i++) {
		sum += arr[i] * arr[i];
	}

	return sqrt(sum);
}

double innerProduct(double vector1[], double vector2[], int length) {
	double prod = 0;
	int i = 0;
	for (i = 0; i < length; i++) {
		prod += vector1[i] * vector2[i];
	}

	return prod;
}

int arraysEqual(double vector1[], double vector2[], int length) {
	double tolerance = 0.00001;
	int ret = TRUE;
	int i;
	for (i = 0; i < length; i++) {
		if ((abs(vector1[i] - vector2[i]) - tolerance) > 0.0) {
			ret = FALSE;
			break;
		}
	}

	return ret;
}

double calculateSigma(int featureLength, double features[], double weights[], double sigmaFactor) {
	double inner = innerProduct(features, weights, featureLength);
	return sigmaFactor*exp(inner);
}

/*
 calculate an
 based on the current features (our state representation)
 and the mean and sigma vectors for the specified wheel. Calculates using a gaussian
 function.

 parameters:
 double[] features -
 double[] meanWeights - the weights used to approximate the mean of the gaussian
 double[] sigmaWeights - the weights used to approximate the sigma of the gaussian
 double * pMean - returns the mean
 double * pSigma - returns the sigma
 returns:
 short - each wheel takes a 16 bit signed integer to specify speed in mm/s
 */
short calculateAction(int featureLength, double features[], double meanWeights[],
		double sigmaWeights[], double * pMean, double * pSigma, double sigmaFactor) {
	*pMean = 0.0;
	*pSigma = 100.0;
	static int initialized = FALSE;
	static int seed = 0;
	if(!initialized) {
		initialized = TRUE;
		seed = (int)time(NULL);
	}
	short action;

	*pMean = innerProduct(features, meanWeights, featureLength);
	*pSigma = calculateSigma(featureLength, features, sigmaWeights, sigmaFactor);



	// Calculate an action value using a Gaussian distribution with mean = scalarMean and standard_deviation = scalarSigma
	action = (short) i4_normal_ab((float) *pMean, (float) *pSigma, &seed);

	return action;
}

/*
 del <- r-rExpected + gamma*v^T*x(s')-v^T*x(s)
 */
double calculateDel(int featureLength, double rExpected, double r, double featuresNow[],
		double featuresNext[], double gamma, double criticWeights[]) {

	return r - rExpected
			+ (gamma * innerProduct(featuresNext, criticWeights, featureLength))
			- innerProduct(featuresNow, criticWeights, featureLength);
}

/*
 rExpected <- rExpected + alpha_r*del
 */
double calculateRExpected(double rExpected, double alphaR, double del) {
	rExpected = rExpected + alphaR * del;
	return rExpected;
}

/*
 e_v <- gamma * lambda * e_v + x(s)
 */
void updateCriticTraces(int featureLength, double gamma, double lambda, double criticTraces[],
		double features[]) {
	double factor = gamma * lambda;

	int i;
	for (i = 0; i < featureLength; i++) {
		criticTraces[i] = factor * criticTraces[i] + features[i];
	}
}

/*
 when updating the weight vectors we should divide alpha by the norm of the feature vector
 v <- v + alpha_v*del*e_v; actually I think for AC-S we define alpha_v=alpha_u*sigma^2 (the variance). Since we have two variances we will need to consider this further
 */
void updateCriticWeights(int featureLength, double criticWeights[], double alpha, double del,
		double criticTraces[], double features[]) {
	double normVal = norm(features, featureLength);
	double factor = alpha * del;
	if (normVal > 0) {
		factor /= normVal;
	}
	int i;
	for (i = 0; i < featureLength; i++) {
		criticWeights[i] += factor * criticTraces[i];
	}
}

/**
 * Notes:
 * Originally the features were only the thresholded cliff sensors. When they
 * were on the circle the feature vector was all zeros. This resulted in the
 * traces never being updated. Going to try adding a bias feature.
 */
void updateActorTraces(int featureLength, double meanTraces[], double mean, double sigmaTraces[],
		double sigma, double features[], short action, double gamma,
		double lambda) {
	/*
	 e_u <- gamma*lambda*e_u+compatible features; Need to look into theses traces more.
	 mean compatible features: (1/sigma^2)*(action-mu(s))*features(s)
	 sigma compatible features: (((action-mu(s))^2/sigma(s)^2)-1)*features(s)
	 */
	double factor = gamma * lambda;
	double meanCompatible =  ((double) action - mean);
//	printf("meanCompatible: %f\n", meanCompatible);
	double sigmaCompatible = (((double) action - mean)
			* ((double) action - mean)) -  (sigma * sigma);
//	printf("sigmaCompatible: %f\n", sigmaCompatible);
	int i;
	for (i = 0; i < featureLength; i++) {
		meanTraces[i] = meanTraces[i] * factor + meanCompatible * features[i];
		sigmaTraces[i] = sigmaTraces[i] * factor
				+ sigmaCompatible * features[i];
	}
}

void updateActorWeights(int featureLength, double meanWeights[], double sigmaWeights[],
		double alphaMean, double del, double meanTraces[], double sigmaTraces[], double alphaSigma,
		double features[]) {
	// u <- u + alpha_u*del*e_u
	double normVal = norm(features, featureLength);
	double factorMean = alphaMean * del;
	double factorSigma = alphaSigma * del;
	if (normVal > 0) {
		factorMean /= normVal;
		factorSigma /= normVal;
	}
	int i;
	for (i = 0; i < featureLength; i++) {
		meanWeights[i] += factorMean * meanTraces[i];
		sigmaWeights[i] += factorSigma * sigmaTraces[i];
	}

	printf("del: %f, sigmaWeights: %f, sigmaTraces: %f\n", del, sigmaWeights[0], sigmaTraces[0]);
}

void updateActorWeightsS(int featureLength, double meanWeights[], double sigmaWeights[],
		double alphaMean, double del, double meanTraces[], double sigmaTraces[], double alphaSigma,
		double features[], double sigma) {
	// u <- u + alpha_u*del*e_u
	double normVal = norm(features, featureLength);
	double factorMean = alphaMean * del * sigma * sigma;
	double factorSigma = alphaSigma * del;
	if (normVal > 0) {
		factorMean /= normVal;
		factorSigma /= normVal;
	}
	int i;
	for (i = 0; i < featureLength; i++) {
		meanWeights[i] += factorMean * meanTraces[i];
		sigmaWeights[i] += factorSigma * sigmaTraces[i];
	}

	printf("del: %f, sigmaWeights: %f, sigmaTraces: %f\n", del, sigmaWeights[0], sigmaTraces[0]);
}

int testCalculateSigma() {
	printf("testCalculateSigma\n");
	int ret = TRUE;
	int featureLength = 1;
	double weights[] = {-0.003};
	double features[] = {1.0};
	double sigma = calculateSigma(featureLength, features, weights, 100.0);
	double expected = 199.4008991006746;
	if(sigma - expected > 0.00000001) {
		printf("Expected sigma: %f, got %f\n", expected, sigma);
		ret = FALSE;
	}
	return ret;
}

int testCalculateAction() {
	printf("testCalculateAction\n");
	int featureLength = 4;
	double features[] = { 1, 0, 1, 1 };
	double meanWeights[] = { 1, 2, -3, 4 };
	double sigmaWeights[] = { 0, 0, 0, 0 };
	double mean = 0.0;
	double sigma = 0.0;
	calculateAction(featureLength, features, meanWeights, sigmaWeights, &mean, &sigma, 100.0);

	int ret = TRUE;
	if (mean != 2) {
		printf("Expected mean: %f, got %f\n", 2.0, mean);
		ret = FALSE;
	}

	const double expectedSigma = 200.0;
	// using a fixed value at the moment
	if (sigma != expectedSigma) {
		printf("Expected sigma: %f, got %f\n", expectedSigma, sigma);
		ret = FALSE;
	}

	return ret;
}

int testCalculateDel() {
	printf("testCalculateDel\n");
	int featureLength = 4;
	int ret = TRUE;
	double rExpected = 1.0;
	double r = 2.0;
	double featuresNow[] = { 1, 1, 0, 0 };
	double featuresNext[] = { 0, 0, 1, 1 };
	double gamma = 0.9;
	double criticWeights[] = { 1, -3, 6, -2 };
	double del = calculateDel(featureLength, rExpected, r, featuresNow, featuresNext, gamma,
			criticWeights);
	double expected = 6.6;
	if (del != expected) {
		ret = FALSE;
		printf("Expected del: %f, got %f\n", expected, del);
	}

	return ret;
}

int testUpdateCriticTraces() {
	printf("testUpdateCriticTraces\n");
	int ret = TRUE;
	int featureLength = 4;
	//gamma * lambda * e_v + x(s)
	double gamma = 0.9;
	double lambda = 0.1;
	double traces[] = { 1, 0.2, 0.3, 4 };
	double features[] = { 0, 0, 1, 1 };
	double newTraces[] = { 0.09, 0.018, 1.027, 1.36 };

	updateCriticTraces(featureLength, gamma, lambda, traces, features);

	if (arraysEqual(newTraces, traces, featureLength) == FALSE) {
		ret = FALSE;
		printf("Arrays did not match\n");
	}

	return ret;
}

int testUpdateCriticWeights() {
	printf("testUpdateCriticWeights\n");
	int featureLength = 4;
	int ret = TRUE;
	double weights[] = { 0.2, -0.2, 3, -1 };
	double newWeights[] = { 0.22, -0.188, 3.006, -0.9 };
	double alpha = 0.01;
	double del = 2.0;
	double traces[] = { 1, 0.6, 0.3, 5 };
	double features[] = { 1, 1, 1, 1 };
	updateCriticWeights(featureLength, weights, alpha, del, traces, features);

	if (arraysEqual(weights, newWeights, featureLength) == FALSE) {
		ret = FALSE;
		printf("Arrays not equal\n");
	}
	return ret;
}

int testUpdateActorTraces() {
	printf("testUpdateActorTraces\n");
	int ret = TRUE;
	int featureLength = 4;
	double meanTraces[] = { 0.2, 1, 3, 2 };
	double newTraces[] = { 0.468, 0.54, 0.27, 0.18 };
	double mean = 20;
	double sigmaTraces[] = { 1, 1, 1, 1 };
	double newSigmaTraces[] = {80.09, 80.09, 0.09, 0.09};
	double sigma = 20;
	double features[] = { 1, 1, 0, 0 };
	short action = 200;
	double gamma = 0.9;
	double lambda = 0.1;
	updateActorTraces(featureLength, meanTraces, mean, sigmaTraces, sigma, features, action,
			gamma, lambda);

	if (arraysEqual(newTraces, meanTraces, featureLength) == FALSE) {
		printf("Mean Traces not equal\n");
		ret = FALSE;
	}

	if (!arraysEqual(newSigmaTraces, sigmaTraces, featureLength)) {
		printf("Sigma Traces not equal\n");
		ret = FALSE;
	}

	// ignoring sigma for now

	return ret;
}

int testUpdateActorWeights() {
	// u <- u + alpha_u*del*e_u
	printf("testUpdateActorWeights\n");
	int ret = TRUE;
	int featureLength = 4;
	double meanWeights[] = { 1, -1, 2, 2 };
	double newMeanWeights[] = { 1.2, -0.84, 2.6, 2.8 };
	double sigmaWeights[] = { 0, 0, 0, 0 };
	double newSigmaWeights[] = {0.02, 0.02, 0.02, 0.02};
	double alpha_u = 0.1;
	double alpha_s = 0.01;
	double del = 2;
	double meanTraces[] = { 1, 0.8, 3, 4 };
	double sigmaTraces[] = { 1, 1, 1, 1 };
	double features[] = { 1, 1, 1, 1 };
	updateActorWeights(featureLength, meanWeights, sigmaWeights, alpha_u, del, meanTraces,
			sigmaTraces, alpha_s, features);

	if (arraysEqual(newMeanWeights, meanWeights, featureLength) == FALSE) {
		printf("Mean Weights not equal\n");
		ret = FALSE;
	}

	if(!arraysEqual(newSigmaWeights, sigmaWeights, featureLength)) {
		printf("Sigma Weights not equal\n");
		ret = FALSE;
	}
	return ret;
}

void runTests() {
	int errCount = 0;
	errCount += testCalculateSigma() ? 0 : 1;
	errCount += testCalculateAction() ? 0 : 1;
	errCount += testCalculateDel() ? 0 : 1;
	errCount += testUpdateCriticTraces() ? 0 : 1;
	errCount += testUpdateCriticWeights() ? 0 : 1;
	errCount += testUpdateActorTraces() ? 0 : 1;
	errCount += testUpdateActorWeights() ? 0 : 1;

	if (errCount > 0) {
		printf("Error count: %d\n", errCount);
	} else {
		printf("Success\n");
	}
}
