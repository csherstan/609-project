#ifndef ACS_H__
#define ACS_H__

double norm(double * arr, int length);

double innerProduct(double vector1[], double vector2[], int length);

int arraysEqual(double vector1[], double vector2[], int length);

double calculateSigma(int featureLength, double features[], double weights[], double sigmaFactor);

short calculateAction(int featureLength, double features[], double meanWeights[],
		double sigmaWeights[], double * pMean, double * pSigma, double sigmaFactor);

double calculateDel(int featureLength, double rExpected, double r, double featuresNow[],
		double featuresNext[], double gamma, double criticWeights[]);

double calculateRExpected(double rExpected, double alphaR, double del);

void updateCriticTraces(int featureLength, double gamma, double lambda, double criticTraces[],
		double features[]);

void updateCriticWeights(int featureLength, double criticWeights[], double alpha, double del,
		double criticTraces[], double features[]);

void updateActorTraces(int featureLength, double meanTraces[], double mean, double sigmaTraces[],
		double sigma, double features[], short action, double gamma,
		double lambda);

void updateActorWeights(int featureLength, double meanWeights[], double sigmaWeights[],
		double alphaMean, double del, double meanTraces[], double sigmaTraces[], double alphaSigma,
		double features[]);

void updateActorWeightsS(int featureLength, double meanWeights[], double sigmaWeights[],
		double alphaMean, double del, double meanTraces[], double sigmaTraces[], double alphaSigma,
		double features[], double sigma);

#endif
