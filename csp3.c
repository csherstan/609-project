#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <termios.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <float.h>
#include <signal.h>
#include "csp3.h"

//------------------------------------------------------------------
// ---------------          Create codes           -----------------
//------------------------------------------------------------------

#define CREATE_START         128
#define CREATE_SAFE          131
#define CREATE_FULL          132
#define CREATE_SPOT          134
#define CREATE_DRIVE         137
#define CREATE_SONG          140
#define CREATE_PLAY          141
#define CREATE_SENSORS       142
#define CREATE_DRIVE_DIRECT  145
#define CREATE_DIGITAL_OUTS  147
#define CREATE_STREAM        148
#define CREATE_STREAM_PAUSE  150

static int SENSOR_PACKET_SIZE[43] = { 26, //0
		10, //1
		6,  //2
		10, //3
		14, //4
		12, //5
		52,	// all
		1, // wheelDrop
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, // distance
		2, // angle
		1, 2,	// voltage
		2,	// current
		1,	//battTemp
		2,	//battCharge
		2,	//battCapacity
		2,	//wallSignal
		2,	//cliffLeftSignal
		2,	//cliffFrontLeftSignal
		2,	//cliffFrontRightSignal
		2,	//cliffRightSignal
		1,	//userDigitalInputs
		2,	//userAnalogInputs
		1,	//charginSourcesAvailable
		1,	//oiMode
		1,	//songNumber
		1,	//songPlaying
		1,	//numberOfStreamPackets
		2,	//velocity
		2,	//radius
		2,	// rightVelocity
		2	// Left Velocity
		};

//------------------------------------------------------------------
// ---------          Global Names and Variables           ---------
//------------------------------------------------------------------
#define SPEED 300
unsigned int pktNum = 0; // Number of the packet currently being constructed by csp3
pthread_mutex_t pktNumMutex, rewardMusicMutex, actionMutex; // locks
#define ACTION_BUFFER_SIZE 100
ubyte action[ACTION_BUFFER_SIZE];
int actionLength = 0;
struct timeval lastPktTime;   // time of last packet
int rewardMusic = 0;
int fd = 0;                   // file descriptor for serial port
volatile pthread_t tid;
static int model;
static csp3Sensors * sensors;
static int sensorCount;
static int sensorPacketSize;

int runCSP3 = TRUE;

//sensory arrays:
#define M 1000

Packet packets[M];

int cliffThresholds[4];       // left, front left, front right, right
int cliffHighValue;           // binary value taken if threshold exceeded

//void cleanupMutex(void * mutex) {
//	pthread_mutex_unlock(mutex);
//}

//------------------------------------------------------------------
/*
 parameters:
 action - this should not be dynamically allocated. Memory will not be freed.
 length - number of elements
 */
void pushAction(ubyte* actionSrc, int length) {
	pthread_mutex_lock(&actionMutex);
	memset(action, 0, ACTION_BUFFER_SIZE * sizeof(ubyte));
	memcpy(action, actionSrc, length * sizeof(ubyte));
	actionLength = length * sizeof(ubyte);
	pthread_mutex_unlock(&actionMutex);

}

int getPktNum() {
	int myPktNum;
	pthread_mutex_lock(&pktNumMutex);
	myPktNum = pktNum - 1;
	pthread_mutex_unlock(&pktNumMutex);
	return myPktNum;
}

int indexByPkt(int pkt) {
	return (pkt + M - 1) % M;
}

Packet getPacket(int pkt) {
	// need to handle pkt == -1 better than this
	if(pkt == -1) {
		pkt = 0;
	}
	return packets[pkt % M];
}

int sendBytesToRobot(ubyte* bytes, int numBytes) {
	int ret;
	int i;
//	printf("sendBytesToRobot: ");
//	for(i = 0; i < numBytes; i++) {
//		printf("%u,", bytes[i]);
//	}
//	printf("\n");
	if ((ret = write(fd, bytes, numBytes)) == -1) {
		fprintf(stderr, "Problem with write(): %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	return ret;
}

void ensureTransmitted() {
	int ret;
	if ((ret = tcdrain(fd)) == -1) {
		fprintf(stderr, "Problem with tcdrain(): %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
}

void loadCliffThresholds(char cliffdataPath[]) {
	FILE *fd;
	if ((fd = fopen(cliffdataPath, "r")) == NULL) {
		fprintf(stderr, "Error opening %s file: %s\n", cliffdataPath,
				strerror(errno));
		exit(EXIT_FAILURE);
	}
	fscanf(fd, "%d%d%d%d%d", &cliffHighValue, &cliffThresholds[0],
			&cliffThresholds[1], &cliffThresholds[2], &cliffThresholds[3]);
	fclose(fd);
}

#define MAX(a,b) (a > b?a:b)
#define MIN(a,b) (a < b?a:b)

/*
 This is a direct drive operation.
 */
void driveWheels(int left, int right) {
	int maxSpeed = 200;
	ubyte bytes[5];
	left = MIN(maxSpeed, left);
	left = MAX(-maxSpeed, left);
	right = MIN(maxSpeed, right);
	right = MAX(-maxSpeed, right);
	bytes[0] = CREATE_DRIVE_DIRECT;
	bytes[1] = (right >> 8) & 0x00FF;
	bytes[2] = right & 0x00FF;
	bytes[3] = (left >> 8) & 0x00FF;
	bytes[4] = left & 0x00FF;
	pushAction(bytes, 5);
}

int reflexes() {
	int ret = FALSE;
//	Packet packet = getPacket(pktNum);
//	if (packet.cliffFLSignalB && packet.cliffFRSignalB && packet.cliffLSignalB
//			&& packet.cliffRSignalB) {
//		ubyte bytes[5];
//		bytes[0] = CREATE_DRIVE_DIRECT;
//		bytes[1] = (100 >> 8) & 0x00FF;
//		bytes[2] = 100 & 0x00FF;
//		bytes[3] = (-100 >> 8) & 0x00FF;
//		bytes[4] = -100 & 0x00FF;
//		sendBytesToRobot(bytes, 5);
//		ensureTransmitted();
//		ret = TRUE;
//	}
	return ret;

}

void setLaser(int state) {
	ubyte bytes[] = {147, 0};
	if(state) {
		bytes[1] = 0xFF;
	}

	pushAction(bytes, 2);
//	sendBytesToRobot(bytes, 2);
}

void setupSerialPort(char serialPortName[]) {
	struct termios options;
	ubyte byte;
	ubyte * bytes = malloc(sizeof(ubyte) * sensorCount + 2);

	// open connection
	if ((fd = open(serialPortName, O_RDWR | O_NOCTTY | O_NONBLOCK)) == -1) {
		fprintf(stderr, "Serial port at %s could not be opened: %s\n",
				serialPortName, strerror(errno));
		exit(EXIT_FAILURE);
	}
	tcflush(fd, TCIOFLUSH);
	tcgetattr(fd, &options);
	options.c_iflag = IGNBRK | IGNPAR;
	options.c_lflag = 0;
	options.c_oflag = 0;
	options.c_cflag = CREAD | CS8 | CLOCAL; // CLOCAL not needed for cu.portname
	// use different baud for 500 series roombas
	switch (model) {
	case CSP3_ROOMBA_500:
		cfsetispeed(&options, B115200);
		cfsetospeed(&options, B115200);
		break;
	case CSP3_CREATE:
	default:
		cfsetispeed(&options, B57600);
		cfsetospeed(&options, B57600);
		break;
	}

	tcsetattr(fd, TCSANOW, &options);
	// go to passive mode:
	byte = CREATE_START;
	sendBytesToRobot(&byte, 1);
	// go to full mode:
	byte = CREATE_FULL;
	sendBytesToRobot(&byte, 1);

	// Request stream mode:
	bytes[0] = CREATE_STREAM;
	bytes[1] = sensorCount * sizeof(ubyte);
	int i;
	for (i = 0; i < sensorCount; i++) {
		bytes[i + 2] = sensors[i];
	}
	sendBytesToRobot(bytes, sizeof(ubyte) * (2 + sensorCount));
	// Setup songs
	bytes[0] = CREATE_SONG;
	bytes[1] = 0;
	bytes[2] = 1;
	bytes[3] = 100;
	bytes[4] = 6;
	sendBytesToRobot(bytes, 5);
	ensureTransmitted();
	free(bytes);
}

// expects (19) (SENSOR_SIZE-3) (SENSOR_CLIFF_LEFT_SIGNAL) () () ... (checksum)
int checkPacket(ubyte packet[]) {
	int i, sum;
	// TODO: we could check that the length and all the headers are what we expect, but I
	// don't think it's necessary
	if (packet[0] == 19) {
		int length = packet[1];
		sum = 0;
		for (i = 0; i < (length + 3); i++)
			sum += packet[i];
		if ((sum & 0xFF) == 0)
			return 1;
	}
	return 0;
}

void extractPacket(ubyte packet[]) {
	struct timeval currentTime;
	int p = pktNum % M;
	int count = packet[1];
	int pointer = 2;

	while (pointer - 2 < count) {
		csp3Sensors sensor = (csp3Sensors) packet[pointer++];
		switch (sensor) {
		case WHEEL_DROP:
			packets[p].wheelDrop = (ubyte) packet[pointer++];
			break;
		case IR:
			packets[p].IRbyte = packet[pointer++];
			break;
		case DISTANCE:
			packets[p].distance = packet[pointer] << 8 | packet[pointer + 1];
			pointer += 2;
			break;
		case ANGLE:
			break;
		case CLIFF_LEFT_SIGNAL:
			packets[p].cliffLSignal = packet[pointer] << 8
					| packet[pointer + 1];
			packets[p].cliffLSignalB =
					packets[p].cliffLSignal > cliffThresholds[0] ?
							cliffHighValue : 1 - cliffHighValue;
			pointer += 2;
			break;
		case CLIFF_FRONT_LEFT_SIGNAL:
			packets[p].cliffFLSignal = packet[pointer] << 8
					| packet[pointer + 1];
			packets[p].cliffFLSignalB =
					packets[p].cliffFLSignal > cliffThresholds[1] ?
							cliffHighValue : 1 - cliffHighValue;
			pointer += 2;
			break;
		case CLIFF_FRONT_RIGHT_SIGNAL:
			packets[p].cliffFRSignal = packet[pointer] << 8
					| packet[pointer + 1];
			packets[p].cliffFRSignalB =
					packets[p].cliffFRSignal > cliffThresholds[2] ?
							cliffHighValue : 1 - cliffHighValue;
			pointer += 2;
			break;
		case CLIFF_RIGHT_SIGNAL:
			packets[p].cliffRSignal = packet[pointer] << 8
					| packet[pointer + 1];
			packets[p].cliffRSignalB =
					packets[p].cliffRSignal > cliffThresholds[3] ?
							cliffHighValue : 1 - cliffHighValue;
			pointer += 2;
			break;
		case WALL_SIGNAL:
			packets[p].wallSignal = packet[pointer] << 8 | packet[pointer + 1];
			pointer +=2;
			break;
		case WALL:
			packets[p].wall = packet[pointer++];
			break;
		case VIRTUAL_WALL:
			packets[p].virtualWall = packet[pointer++];
			break;
		case REQUESTED_LEFT_VELOCITY:
			packets[p].requestedLeftVelocity = packet[pointer] << 8 | packet [pointer + 1];
			pointer +=2;
			break;
		case REQUESTED_RIGHT_VELOCITY:
			packets[p].requestedRightVelocity = packet[pointer] << 8 | packet[pointer + 1];
			pointer +=2;
			break;
		default:
			pointer += SENSOR_PACKET_SIZE[sensor];
			break;
		}
	}

	gettimeofday(&currentTime, NULL);
	packets[p].deltaT = (currentTime.tv_sec - lastPktTime.tv_sec) * 1000
			+ ((double) currentTime.tv_usec - lastPktTime.tv_usec) / 1000;
	lastPktTime = currentTime;
//	printf(
//			"distance: %u, castor: %u, left: %u, right: %u, wheelDrop: %u, cdropL: %u, cdropLF: %u, cdropRF: %u, cdropR: %u\n",
//			packets[p].distance,
//			packets[p].wheelDropCaster, packets[p].wheelDropLeft,
//			packets[p].wheelDropRight, packets[p].wheelDrop,
//			packets[p].cliffLSignal, packets[p].cliffFLSignal,
//			packets[p].cliffFRSignal, packets[p].cliffRSignal);

//	printf("****p: %d, wheel drop: %u, cliff sensors: %u %u %u %u, distance: %d\n", p, packets[p].wheelDropCaster, packets[p].cliffLSignal, packets[p].cliffFLSignal,
//			packets[p].cliffFRSignal, packets[p].cliffRSignal,packets[p].distance);
}

void takeAction() {
	if (!reflexes()) {
		pthread_mutex_lock(&actionMutex);
		if (actionLength > 0) {
			sendBytesToRobot(action, actionLength);
			ensureTransmitted();
			actionLength = 0;
		}
		pthread_mutex_unlock(&actionMutex);
	}
}

int calcSensorLength() {
	int length = 0;
	int i;
	for (i = 0; i < sensorCount; i++) {
		length++;
		length += SENSOR_PACKET_SIZE[sensors[i]];
	}

	return length + 3;
}
void initCSP3(char portName[], char cliffdataPath[], int modelConf,
		csp3Sensors sensorList[], int sensorLength) {
	pthread_mutex_init(&pktNumMutex, NULL);
	pthread_mutex_init(&rewardMusicMutex, NULL);
	pthread_mutex_init(&actionMutex, NULL);
	model = modelConf;
	sensors = sensorList;
	sensorCount = sensorLength;
	sensorPacketSize = calcSensorLength();
	setupSerialPort(portName);
	loadCliffThresholds(cliffdataPath);

	int t_err = pthread_create(&tid, NULL, &csp3, NULL);
	if (t_err != 0) {
		fprintf(stderr, "\ncan't create thread: [%s]", strerror(t_err));
		exit(EXIT_FAILURE);
	}
}

void endCSP3() {
	pthread_cancel(tid);
}

// TODO: need to handle exiting properly. This is particularly important for the 500 series roombas,
// which do not respond to their on/off button once they are in full mode.
void terminate(void * arg) {
	ubyte bytes[2];
	printf("Ending Program\n");
	driveWheels(0, 0);
	takeAction();
	// pause streaming
	bytes[0] = CREATE_STREAM_PAUSE;
	bytes[1] = 0;
	sendBytesToRobot(bytes, 2);
	tcdrain(fd);
	close(fd);
}

void* csp3(void *arg) {
	pthread_cleanup_push(&terminate, NULL);
	int errorCode, numBytesRead;
	ubyte bytes[sensorPacketSize];
	int numBytesPreviouslyRead = 0;
	struct timeval timeout;
	fd_set readfs;

	gettimeofday(&lastPktTime, NULL);
	FD_SET(fd, &readfs);

	while (runCSP3) {
		timeout.tv_sec = 2;
		timeout.tv_usec = 0;
		errorCode = select(fd + 1, &readfs, NULL, NULL, &timeout);
		if (errorCode == 0) {
			printf("Timed out at select()\n");
		} else if (errorCode == -1) {
			fprintf(stderr, "Problem with select(): %s\n", strerror(errno));
			exit(EXIT_FAILURE);
		}
		numBytesRead = read(fd, bytes + numBytesPreviouslyRead,
				sensorPacketSize - numBytesPreviouslyRead);
		if (numBytesRead == -1) {
			fprintf(stderr, "Problem with read(): %s\n", strerror(errno));
			exit(EXIT_FAILURE);
		} else {
			numBytesPreviouslyRead += numBytesRead;
			if (numBytesPreviouslyRead == sensorPacketSize) { //packet complete!
				if (checkPacket(bytes)) {
					extractPacket(bytes);
					takeAction();
					pthread_mutex_lock(&pktNumMutex);
					pktNum++;
//					printf("pktNum: %d\n", pktNum);
					pthread_mutex_unlock(&pktNumMutex);
					numBytesPreviouslyRead = 0;
				} else {
					printf("misaligned packet.\n");
					memcpy(bytes, bytes + 1,
							sizeof(ubyte) * (sensorPacketSize - 1));
					numBytesPreviouslyRead--;
				}
			}
		}

		usleep(100);
	}

	pthread_cleanup_pop(0);

	return NULL;
}

