#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <termios.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <float.h>
#include <signal.h>
#include "csp3.h"
#include "normal.h"
#include <time.h>
#include "math.h"
#include "acs.h"

int runProgram = TRUE;

/*
 Returns a set of features based on the current read observation.
 In order to deal with delay we may want to incorporate previous
 readings as well.
 */
void getFeatures(int pktNum, double features[], int featureLength) {
	features[0] = 1.0;
}

/*
 Not sure how this is supposed to work, see sarsa.c

 The encoders are not very fine grained. If we are moving slowly then the distance
 will show up as 1 or 0 even though it's clearly moving more than that.
 So, don't use the average distance, but rather the combined distance over all
 the packets.
 */
double calculateReward(int pktStart, int pktStop) {
	Packet packet = getPacket(pktStop);
	return -1
			* (abs(packet.requestedLeftVelocity)
					+ abs(packet.requestedRightVelocity)) / 100.0;
}

void drive(int left, int right) {
	driveWheels(left, right);
}

typedef struct Result {
	double reward;
	double leftMean;
	double leftSigma;
	double rightMean;
	double rightSigma;
	int leftAction;
	int rightAction;
} Result;

void writeResults(char filename[], Result results[], int length) {
	FILE * fp = fopen(filename, "w");
	int i;
	fprintf(fp,
			"reward, leftAction, leftMean, leftSigma, rightAction, rightMean, rightSigma\n");
	for (i = 0; i < length; i++) {
		Result result = results[i];
		fprintf(fp, "%f, %d, %f, %f, %d, %f, %f\n", result.reward,
				result.leftAction, result.leftMean, result.leftSigma,
				result.rightAction, result.rightMean, result.rightSigma);
	}
	fclose(fp);
}

void actorCritic() {
	/*
	 * Rule of thumb - update of actor mean 10 times less than update of critic, update of actor sigma
	 * 10 times less than update of actor mean
	 */
	double alpha_r = 0.3;   // expected R learning rate
	double alpha_v = 0.1;   //critic learning rate
	double alpha_u = 0.001;  //actor mean learning rate
	double alpha_s = 0.0005;	//actor sigma learning rate
	double gamma = 1;
	double lambda = 0.9;
	static const double sigmaZero = 20;
	int delay = 5;
	int accumulate = 5;

	int featureLength = 1;

	int testSize = 500;
	Result results[testSize];

	double featuresNow[featureLength];
	double featuresNext[featureLength];
	// so far I have initialized all the weights to zeros, this might not be a good idea.
	// might want to initialize to some small values
	double actorWeightsLeftMean[featureLength];
	memset(actorWeightsLeftMean, 0, featureLength * sizeof(double));
	double actorWeightsRightMean[featureLength];
	memset(actorWeightsRightMean, 0, featureLength * sizeof(double));
	double actorTracesLeftMean[featureLength];
	memset(actorTracesLeftMean, 0, featureLength * sizeof(double));
	double actorTracesRightMean[featureLength];
	memset(actorTracesRightMean, 0, featureLength * sizeof(double));

	// for now we're not using these, we will start with fixed value of sigma
	double actorWeightsLeftSigma[featureLength];
	memset(actorWeightsLeftSigma, 0, featureLength * sizeof(double));
	double actorWeightsRightSigma[featureLength];
	memset(actorWeightsRightSigma, 0, featureLength * sizeof(double));
	double actorTracesLeftSigma[featureLength];
	memset(actorTracesLeftSigma, 0, featureLength * sizeof(double));
	double actorTracesRightSigma[featureLength];
	memset(actorTracesRightSigma, 0, featureLength * sizeof(double));

	// this is vector "v" used in the paper
	double criticWeights[featureLength];
	memset(criticWeights, 0, featureLength * sizeof(double));

	// e^v in the paper
	double criticTraces[featureLength];
	memset(criticTraces, 0, featureLength * sizeof(double));

	double rExpected = 0.0;
	int prevPktNum = 0; // this will be used for calculating reward.
	int myPktNum = getPktNum();
	double leftMean = 0.0;
	double leftSigma = 1.0;
	double rightMean = 0.0;
	double rightSigma = 1.0;

	int iter = 0;
	getFeatures(myPktNum, featuresNow, featureLength);
	while (runProgram && iter < testSize) {
		printf("iter: %d\n", iter);
		prevPktNum = myPktNum;

		int left = calculateAction(featureLength, featuresNow,
				actorWeightsLeftMean, actorWeightsLeftSigma, &leftMean,
				&leftSigma, sigmaZero);
		int right = calculateAction(featureLength, featuresNow,
				actorWeightsRightMean, actorWeightsRightSigma, &rightMean,
				&rightSigma, sigmaZero);
		printf("leftMean: %f, leftSigma: %f, rightMean: %f, rightSigma: %f\n",
				leftMean, leftSigma, rightMean, rightSigma);
		printf("left: %d, right: %d\n", left, right);
		drive(left, right);
		// now we need to delay until we have some new data for now let's use a fixed delay
		while ((myPktNum = getPktNum()) - prevPktNum < (accumulate + delay)) {
			usleep(5000);
		}
		myPktNum = getPktNum();
		getFeatures(myPktNum, featuresNext, featureLength);
		double r = calculateReward(prevPktNum + delay + 1, myPktNum);
		results[iter].leftMean = leftMean;
		results[iter].leftAction = left;
		results[iter].leftSigma = leftSigma;
		results[iter].reward = r;
		results[iter].rightMean = rightMean;
		results[iter].rightAction = right;
		results[iter].rightSigma = rightSigma;
		double del = calculateDel(featureLength, rExpected, r, featuresNow,
				featuresNext, gamma, criticWeights);
		printf("r: %f, del: %f, expected: %f\n", r, del, rExpected);
		rExpected = calculateRExpected(rExpected, alpha_r, del);
//		printf("newExpected: %f\n", rExpected);
		updateCriticTraces(featureLength, gamma, lambda, criticTraces,
				featuresNow);
		updateCriticWeights(featureLength, criticWeights, alpha_v, del,
				criticTraces, featuresNow);
		updateActorTraces(featureLength, actorTracesLeftMean, leftMean,
				actorTracesLeftSigma, leftSigma, featuresNow, left, gamma,
				lambda);
		updateActorTraces(featureLength, actorTracesRightMean, rightMean,
				actorTracesRightSigma, rightSigma, featuresNow, right, gamma,
				lambda);
		updateActorWeights(featureLength, actorWeightsLeftMean,
				actorWeightsLeftSigma, alpha_u, del, actorTracesLeftMean,
				actorTracesLeftSigma, alpha_s, featuresNow);
		updateActorWeights(featureLength, actorWeightsRightMean,
				actorWeightsRightSigma, alpha_u, del, actorTracesRightMean,
				actorTracesRightSigma, alpha_s, featuresNow);
		iter++;
	}

	writeResults("acs_stand_still.csv", results, testSize);
}

void endProgram() {
	runProgram = FALSE;
	endCSP3();
	exit(EXIT_SUCCESS);
}

/**
 *
 */
int main(int argc, char *argv[]) {

	signal(SIGINT, &endProgram);
	signal(SIGTSTP, &endProgram);
	signal(SIGQUIT, &endProgram);
	signal(SIGTERM, &endProgram);
	signal(SIGHUP, &endProgram);

	// consider splitting this framework stuff from the actor critic bit
	if (argc < 2) {
		fprintf(stderr,
				"Portname argument required -- something like /dev/tty.usbserial\n");
		return 0;
	}

	int model = CSP3_CREATE;
	int opt = getopt(argc, argv, "m:");
	while (-1 != opt) {
		switch (opt) {
		case 'm':
			if (strcmp(optarg, "500") == 0) {
				model = CSP3_ROOMBA_500;
			}
			break;
		}
	}

	srand(0);

	csp3Sensors sensors[] = { WHEEL_DROP, CLIFF_LEFT_SIGNAL,
			CLIFF_FRONT_LEFT_SIGNAL, CLIFF_FRONT_RIGHT_SIGNAL,
			CLIFF_RIGHT_SIGNAL, DISTANCE, IR, REQUESTED_LEFT_VELOCITY,
			REQUESTED_RIGHT_VELOCITY };

	initCSP3(argv[1], argv[2], model, sensors, sizeof(sensors) / sizeof(int));
	usleep(20000); // wait for at least one packet to have arrived

	actorCritic();

	endCSP3();

	return 0;
}
