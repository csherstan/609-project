#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <termios.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <float.h>
#include <signal.h>

typedef unsigned char ubyte;

#define CSP3_CREATE 1
#define CSP3_ROOMBA_500 2

#define CREATE_START         128
#define CREATE_SAFE          131
#define CREATE_FULL          132
#define CREATE_SPOT          134
#define CREATE_DRIVE         137
#define CREATE_DRIVE_DIRECT  145
#define CREATE_POWER		 133

int sendBytesToRobot(int fd, ubyte* bytes, int numBytes) {
	int ret;
	if ((ret = write(fd, bytes, numBytes)) == -1) {
		fprintf(stderr, "Problem with write(): %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	return ret;
}
int main(int argc, char *argv[]) {

	char * serialPortName = argv[1];
	struct termios options;
	ubyte byte;
	int fd = 0;
	// open connection
	if ((fd = open(serialPortName, O_RDWR | O_NOCTTY | O_NONBLOCK)) == -1) {
		fprintf(stderr, "Serial port at %s could not be opened: %s\n",
				serialPortName, strerror(errno));
		exit(EXIT_FAILURE);
	}
	tcflush(fd, TCIOFLUSH);
	tcgetattr(fd, &options);
	options.c_iflag = IGNBRK | IGNPAR;
	options.c_lflag = 0;
	options.c_oflag = 0;
	options.c_cflag = CREAD | CS8 | CLOCAL; // CLOCAL not needed for cu.portname
	// use different baud for 500 series roombas
//	switch (model) {
//	case CSP3_ROOMBA_500:
	cfsetispeed(&options, B115200);
	cfsetospeed(&options, B115200);
//		break;
//	case CSP3_CREATE:
//	default:
//		cfsetispeed(&options, B57600);
//		cfsetospeed(&options, B57600);
//		break;
//	}

	tcsetattr(fd, TCSANOW, &options);
	// go to passive mode:
	byte = CREATE_START;
	sendBytesToRobot(fd, &byte, 1);
	// go to full mode:
	byte = CREATE_POWER;
	sendBytesToRobot(fd, &byte, 1);

	return 0;
}
