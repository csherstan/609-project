#ifndef CSP3_H_
#define CSP3_H_
typedef unsigned char ubyte;

typedef struct Packet {
	unsigned short cliffLSignal; // small pos integers
	unsigned short cliffRSignal;
	unsigned short cliffFLSignal;
	unsigned short cliffFRSignal;
	ubyte cliffLSignalB; // binary 1/0
	ubyte cliffRSignalB;
	ubyte cliffFLSignalB;
	ubyte cliffFRSignalB;
	short distance; // wheel rotation counts (small integers, pos/neg)
	double deltaT; // in milliseconds
	ubyte IRbyte; // Infrared byte e.g. remote
	ubyte drive; // Drive command in {0, 1, 2, 3, 4}

	union {
		ubyte wheelDrop;
		struct {
			ubyte bumpRight :1;
			ubyte bumpLeft :1;
			ubyte wheelDropRight :1;
			ubyte wheelDropLeft :1;
			ubyte wheelDropCaster :1;
			ubyte none :2;
		};
	};

	ubyte virtualWall;
	ubyte wall;
	short wallSignal;
	short requestedLeftVelocity;
	short requestedRightVelocity;
} Packet;

typedef enum {
	GROUP_ZERO,
	GROUP_ONE,
	GROUP_TWO,
	GROUP_THREE,
	GROUP_FOUR,
	GROUP_FIVE,
	ALL,
	WHEEL_DROP,
	WALL,
	CLIFF_LEFT,
	CLIFF_FRONT_LEFT,
	CLIFF_FRONT_RIGHT,
	CLIFF_RIGHT,
	VIRTUAL_WALL,
	LOW_SIDE_DRIVER_AND_WHEEL_OVERCURRENTS,
	UNUSED1,
	UNUSED2,
	IR,
	BUTTONS,
	DISTANCE,	//19
	ANGLE,
	CHARGE_STATE,
	VOLTAGE,
	CURRENT,
	BATT_TEMP,
	BATT_CHARGE,
	BATT_CAPACITY,
	WALL_SIGNAL,
	CLIFF_LEFT_SIGNAL,
	CLIFF_FRONT_LEFT_SIGNAL,
	CLIFF_FRONT_RIGHT_SIGNAL,
	CLIFF_RIGHT_SIGNAL,
	CARGO_BAY_DIGITAL_INPUTS,
	CARGO_BAY_ANALOG_SIGNAL,
	CHARGING_SOURCES_AVAILABLE,
	MODE,
	SONG_NUMBER,
	SONG_PLAYING,
	NUMBER_OF_STREAM_PACKETS,
	REQUESTED_VELOCITY,
	REQUESTED_RADIUS,
	REQUESTED_RIGHT_VELOCITY,
	REQUESTED_LEFT_VELOCITY
} csp3Sensors;

typedef enum {
	IR_LEFT = 129,
	IR_FORWARD,
	IR_RIGHT,
	IR_SPOT,
	IR_MAX,
	IR_SMALL,
	IR_MEDIUM,
	IR_LARGE_CLEAN,
	IR_PAUSE,
	IR_POWER,
	IR_ARC_FORWARD_LEFT,
	IR_ARC_FORWARD_RIGHT,
	IR_DRIVE_STOP,
	IR_SEND_ALL,
	IR_SEEK_DOCK = 143,
	IR_R500_FORCE_FIELD = 161,
	IR_VIRTUAL_WALL = 162,
	IR_R500_GREEN_BUOY = 164,
	IR_R500_GREEN_BUOY_AND_FORCEFIELD = 165,
	IR_R500_RED_BUOY = 168,
	IR_R500_RED_BUOY_AND_FORCEFIELD = 169,
	IR_R500_RED_AND_GREEN_BOUY = 172,
	IR_R500_RED_AND_GREEN_BOUY_AND_FORCE_FIELD = 173,
	IR_RED_BUOY = 248,
	IR_GREEN_BUOY = 244,
	IR_FORCE_FIELD = 242,
	IR_RED_AND_GREEN_BUOY = 252,
	IR_RED_BUOY_AND_FORCEFIELD = 250,
	IR_GREEN_BUOY_AND_FORCEFIELD = 246,
	IR_RED_AND_GREEN_BUOY_AND_FORCEFIELD = 254,
	IR_NO_SIGNAL = 255
} csp3IRCode;

#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

void* csp3(void *arg);
void driveWheels(int left, int right);
int getPktNum();
void loadCliffThresholds();
void setReflexes(int (*functionPtr)(int, int));
Packet getPacket(int pkt);
void initCSP3();
void endCSP3();

void setLaser(int state);

// Models
#define CSP3_CREATE 1
#define CSP3_ROOMBA_500	2
#endif
