#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <termios.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <float.h>
#include <signal.h>
#include "csp3.h"

int main(int argc, char *argv[]) {
	pthread_t tid;
	int t_err;

//	FILE *gnuplot = popen("/usr/local/bin/gnuplot -persist", "w");
//	fprintf(gnuplot, "plot '-'\n");
//	for (i = 0; i < count; i++)
//	    fprintf(gnuplot, "%g %g\n", x[i], y[i]);
//	fprintf(gnuplot, "e\n");
//	fflush(gnuplot);

	// consider splitting this framework stuff from the actor critic bit
	if (argc < 2) {
		fprintf(stderr,
				"Portname argument required -- something like /dev/tty.usbserial\n");
		return 0;
	}

	srand(0);
	csp3Sensors sensors [] = {
//			WHEEL_DROP,
//			CLIFF_LEFT_SIGNAL,
//			CLIFF_FRONT_LEFT_SIGNAL,
//			CLIFF_FRONT_RIGHT_SIGNAL,
//			CLIFF_RIGHT_SIGNAL,
			IR,
			WALL,
			VIRTUAL_WALL,
			WALL_SIGNAL,
			DISTANCE,
			REQUESTED_LEFT_VELOCITY,
			REQUESTED_RIGHT_VELOCITY};
	initCSP3(argv[1], argv[2], CSP3_CREATE, sensors, sizeof(sensors)/sizeof(int));
	usleep(50000); // wait for at least one packet to have arrived

	int pktNum = getPktNum();
	int lastPktNum = 0;
//	driveWheels(50, 50);
	while (TRUE) {
//		driveWheels(100,100);
		pktNum = getPktNum();
		lastPktNum = pktNum;

		setLaser(TRUE);
		sleep(5);
		setLaser(FALSE);
		sleep(5);
//		Packet packet;
//		int i = 0;
//		do {
//			pktNum = getPktNum();
//			if(pktNum != lastPktNum) {
//				packet = getPacket(pktNum);
//				printf("pkt: %d, left: %d, right: %d, distance: %d\n", pktNum, packet.requestedLeftVelocity, packet.requestedRightVelocity, packet.distance);
//				fprintf(gnuplot, "%d\n", packet.distance);
//				fflush(gnuplot);
//				lastPktNum = pktNum;
//			}
//			usleep(5000);
//		} while(i < 100);
//
//		printf("%d\n", pktNum - lastPktNum);
//
////		driveWheels(0,0);

//		break;

//		pktNum = getPktNum();
//		if (pktNum != lastPktNum) {
////			printf("*** pktNum: %d\n", pktNum);
//			Packet packet = getPacket(pktNum);
//			lastPktNum = pktNum;
//
////			driveWheels(300, 300);
//			printf("ir: %x, wall: %u, virtual: %u, wall_signal: %d\n", packet.IRbyte, packet.wall, packet.virtualWall, packet.wallSignal);
////			printf("wheel drop: %u, cliff sensors: %u %u %u %u, distance: %d\n", packet.wheelDropCaster, packet.cliffLSignal, packet.cliffFLSignal,
////					packet.cliffFRSignal, packet.cliffRSignal,packet.distance);
//		}
//		usleep(100000);
	}

	return 0;
}
