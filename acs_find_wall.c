#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <termios.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <float.h>
#include <signal.h>
#include "csp3.h"
#include "normal.h"
#include <time.h>
#include "math.h"
#include "acs.h"

static int runProgram = TRUE;
/*
 Returns a set of features based on the current read observation.
 In order to deal with delay we may want to incorporate previous
 readings as well.
 */
void getFeatures(int pktNum, double features[], int featureLength) {
	Packet packet = getPacket(pktNum);
	printf("ir: %u\n", packet.IRbyte);
	features[0] = 1.0;
	int historyLength = 2;
	memcpy(features + historyLength + 1, features + 1,
			sizeof(double) * (featureLength - 1 - historyLength));
	features[1] = packet.IRbyte == IR_R500_GREEN_BUOY
			|| packet.IRbyte == IR_R500_RED_AND_GREEN_BOUY;
	features[2] = packet.IRbyte == IR_R500_RED_BUOY
			|| packet.IRbyte == IR_R500_RED_AND_GREEN_BOUY;
//	features[1] = packet.cliffFLSignalB > 0 ? 1.0 : 0.0;
//	features[2] = packet.cliffFRSignalB > 0 ? 1.0 : 0.0;
//	features[3] = packet.cliffLSignalB > 0 ? 1.0 : 0.0;
//	features[4] = packet.cliffRSignalB > 0 ? 1.0 : 0.0;

	printf("features: ");
	int i;
	for (i = 0; i < featureLength; i++) {
		printf("%f ", features[i]);
	}

	printf("\n");
}

/*
 Not sure how this is supposed to work, see sarsa.c

 The encoders are not very fine grained. If we are moving slowly then the distance
 will show up as 1 or 0 even though it's clearly moving more than that.
 So, don't use the average distance, but rather the combined distance over all
 the packets.
 */
double calculateReward(int pktStart, int pktStop) {
	double reward = 0;
	int i;
	for (i = pktStart; i <= pktStop; i++) {
		Packet packet = getPacket(i);
//		if (packet.distance != 0) {
//			reward -= 10;
//		} else {
		if (packet.IRbyte == IR_R500_GREEN_BUOY
				|| packet.IRbyte == IR_R500_RED_BUOY) {
			reward += 5;
		} else if (packet.IRbyte == IR_R500_RED_AND_GREEN_BOUY
				|| packet.IRbyte
						== IR_R500_RED_AND_GREEN_BOUY_AND_FORCE_FIELD) {
			reward += 100;
		} else {
			reward -= 1;
		}
//		}

//		if (packet.IRbyte == IR_R500_GREEN_BUOY
//				|| packet.IRbyte == IR_R500_GREEN_BUOY) {
//			reward += packet.distance;
//		} else if (packet.IRbyte == IR_R500_RED_AND_GREEN_BOUY) {
//			reward += packet.distance*1.5;
//		} else if (packet.IRbyte == IR_R500_RED_AND_GREEN_BOUY_AND_FORCE_FIELD && packet.distance == 0) {
//			reward += 100;
//		} else {
//			reward -= 1;
//		}
	}
	return reward;
}

void drive(int left, int right) {
	int pktNum = getPktNum();
	Packet packet = getPacket(pktNum);

	if (packet.IRbyte == IR_NO_SIGNAL) {
		left = 300;
		right = -1 * left;
	}

	driveWheels(left, right);
}

void actorCritic() {
	/*
	 * Rule of thumb - update of actor mean 10 times less than update of critic, update of actor sigma
	 * 10 times less than update of actor mean
	 */
	double alpha_r = 0.2;   // expected R learning rate
	double alpha_v = 0.1;   //critic learning rate
	double alpha_u = 0.001;  //actor mean learning rate
	double alpha_s = 0.000001;	//actor sigma learning rate
	double gamma = 1.0;
	double lambda = 0.9;
	double sigmaZero = 20;

	/*
	 * There are 3 packets between when a velocity is sent and the corresponding requestedVelocity updates
	 * On carpet it takes 14 packets to go from 0 to max velocity, 5 packets to go from 0 to some measured distance (at Max speed requested)
	 * In air it takes 8 packets to go from 0 to max velocity, 5 packets to go from 0 to some measured distance (at Max speed requested).
	 * At requested speed 100:
	 * 	On carpet - 5 packets to see a change
	 * 	In air - 5 packets to see a change
	 */
	int accumulate = 5;
	int delay = 5;

	int history = 2;
	int featureLength = 1 + history * 2;

	double featuresNow[featureLength];
	double featuresNext[featureLength];
	// so far I have initialized all the weights to zeros, this might not be a good idea.
	// might want to initialize to some small values
	double actorWeightsLeftMean[featureLength];
	memset(actorWeightsLeftMean, 0, featureLength * sizeof(double));
	double actorWeightsRightMean[featureLength];
	memset(actorWeightsRightMean, 0, featureLength * sizeof(double));
	double actorTracesLeftMean[featureLength];
	memset(actorTracesLeftMean, 0, featureLength * sizeof(double));
	double actorTracesRightMean[featureLength];
	memset(actorTracesRightMean, 0, featureLength * sizeof(double));

	// for now we're not using these, we will start with fixed value of sigma
	double actorWeightsLeftSigma[featureLength];
	memset(actorWeightsLeftSigma, 0, featureLength * sizeof(double));
	double actorWeightsRightSigma[featureLength];
	memset(actorWeightsRightSigma, 0, featureLength * sizeof(double));
	double actorTracesLeftSigma[featureLength];
	memset(actorTracesLeftSigma, 0, featureLength * sizeof(double));
	double actorTracesRightSigma[featureLength];
	memset(actorTracesRightSigma, 0, featureLength * sizeof(double));

	// this is vector "v" used in the paper
	double criticWeights[featureLength];
	memset(criticWeights, 0, featureLength * sizeof(double));

	// e^v in the paper
	double criticTraces[featureLength];
	memset(criticTraces, 0, featureLength * sizeof(double));

	double rExpected = 0.0;
	int prevPktNum = 0; // this will be used for calculating reward.
	int myPktNum = getPktNum();
	double leftMean = 0.0;
	double leftSigma = 1.0;
	double rightMean = 0.0;
	double rightSigma = 1.0;

	int iter = 0;
	getFeatures(myPktNum, featuresNow, featureLength);
	while (runProgram/* && iter < 20*/) {
		prevPktNum = myPktNum;

		int left = calculateAction(featureLength, featuresNow,
				actorWeightsLeftMean, actorWeightsLeftSigma, &leftMean,
				&leftSigma, sigmaZero);
		int right = calculateAction(featureLength, featuresNow,
				actorWeightsRightMean, actorWeightsRightSigma, &rightMean,
				&rightSigma, sigmaZero);
		right = -1 * left;
//		printf("leftMean: %f, leftSigma: %f, rightMean: %f, rightSigma: %f\n",
//				leftMean, leftSigma, rightMean, rightSigma);
//		printf("left: %d, right: %d\n", left, right);
		drive(left, right);
		// now we need to delay until we have some new data for now let's use a fixed delay
		while ((myPktNum = getPktNum()) - prevPktNum < (accumulate + delay)) {
			usleep(5000);
		}
		myPktNum = getPktNum();
		getFeatures(myPktNum, featuresNext, featureLength);
		double r = calculateReward(prevPktNum + delay + 1, myPktNum);
		double del = calculateDel(featureLength, rExpected, r, featuresNow,
				featuresNext, gamma, criticWeights);
//		printf("r: %f, del: %f, expected: %f ", r, del, rExpected);
		rExpected = calculateRExpected(rExpected, alpha_r, del);
//		printf("newExpected: %f\n", rExpected);
		updateCriticTraces(featureLength, gamma, lambda, criticTraces,
				featuresNow);
		updateCriticWeights(featureLength, criticWeights, alpha_v, del,
				criticTraces, featuresNow);
		updateActorTraces(featureLength, actorTracesLeftMean, leftMean,
				actorTracesLeftSigma, leftSigma, featuresNow, left, gamma,
				lambda);
//		updateActorTraces(featureLength, actorTracesRightMean, rightMean,
//				actorTracesRightSigma, rightSigma, featuresNow, right, gamma,
//				lambda);
		updateActorWeights(featureLength, actorWeightsLeftMean,
				actorWeightsLeftSigma, alpha_u, del, actorTracesLeftMean,
				actorTracesLeftSigma, alpha_s, featuresNow);
//		updateActorWeights(featureLength, actorWeightsRightMean, actorWeightsRightSigma,
//				alpha_u, del, actorTracesRightMean, actorTracesRightSigma, alpha_s,
//				featuresNow);

		double testFeatures[] = { 1, 1, 0, 1, 0 };
		printf("testMean: %f, testSigma: %f\n",
				innerProduct(testFeatures, actorWeightsLeftMean, featureLength),
				calculateSigma(featureLength, testFeatures,
						actorWeightsRightMean, sigmaZero));
		iter++;
	}
}

void endProgram() {
	runProgram = FALSE;
	endCSP3();
	exit(EXIT_SUCCESS);
}

double calcLaserReward(int pkt, int laserState) {
	double reward = 0.0;
	Packet packet = getPacket(pkt);
	printf("ir: %u\n", packet.IRbyte);
	if (packet.IRbyte == IR_R500_RED_AND_GREEN_BOUY
			|| packet.IRbyte == IR_R500_RED_AND_GREEN_BOUY_AND_FORCE_FIELD) {
		if (laserState) {
			reward = 5;
		} else {
			reward = -1;
		}
	} else {
		if (laserState) {
			reward = -5;
		} else {
			reward = 1;
		}
	}

	return reward;
}

int getLaserFeatures(int laserState, int pktNum) {
	int state = 0;
	Packet packet = getPacket(pktNum);
	if (laserState) {
		state |= 0x04;
	}
	if (packet.IRbyte == IR_R500_GREEN_BUOY
			|| packet.IRbyte == IR_R500_GREEN_BUOY_AND_FORCEFIELD
			|| packet.IRbyte == IR_R500_RED_AND_GREEN_BOUY
			|| packet.IRbyte == IR_R500_RED_AND_GREEN_BOUY_AND_FORCE_FIELD) {
		state |= 0x02;
	}
	if (packet.IRbyte == IR_R500_RED_BUOY
			|| packet.IRbyte == IR_R500_RED_BUOY_AND_FORCEFIELD
			|| packet.IRbyte == IR_R500_RED_AND_GREEN_BOUY
			|| packet.IRbyte == IR_R500_RED_AND_GREEN_BOUY_AND_FORCE_FIELD) {
		state |= 0x01;
	}

	return state;
}

int epsilonGreedy(double Q[8][2], int s, double epsilon) {
	int max, i, p;
	int firstAction = 0, lastAction = 1;

	if (rand() / ((double) RAND_MAX + 1) < epsilon) {
		printf("random action\n\n");
		return firstAction + rand() % (lastAction + 1 - firstAction);
	} else {
		max = lastAction;
		for (i = firstAction; i < lastAction; i++)
			if (Q[s][i] > Q[s][max])
				max = i;
		return max;
	}
}

typedef struct Result {
	double reward;
	ubyte ir;
	int action;
} Result;

void writeResults(char filename[], Result results[], int length) {
	FILE * fp = fopen(filename, "w");
	int i;
	fprintf(fp, "iter, reward, ir, action\n");
	for (i = 0; i < length; i++) {
		Result result = results[i];
		fprintf(fp, "%d, %f, %d, %d\n", i, result.reward,
				result.ir, result.action);
	}
	fclose(fp);
}

void* laserLearner(void *arg) {
	double epsilon = 0.0;
	double alpha = 0.1;
	double gamma = 0.9;

	int testSize = 300;
	Result results[testSize];

	double Q[8][2];
	double e[8][2];
	int i;
	int j;
	for (i = 0; i < 8; i++) {
		for (j = 0; j < 2; j++) {
			Q[i][j] = 5 + 0.001 * (rand() / ((double) RAND_MAX) - 0.5);
			e[i][j] = 0;
		}
	}
	int myPktNum = 0;
	int prevPktNum = 0;
	int state = 0;
	int nextState = 0;
	int laserState = FALSE;
	int nextAction = FALSE;
	double reward = 0;
	myPktNum = getPktNum();
	state = getLaserFeatures(laserState, myPktNum);
	laserState = epsilonGreedy(Q, state, epsilon);
	printf("action: %u\n", laserState);

	int iter = 0;
	while (TRUE && iter < testSize) {
		prevPktNum = myPktNum;

		setLaser(laserState);
		Packet packet = getPacket(myPktNum);

		while ((myPktNum = getPktNum()) - prevPktNum < (1 + 3)) {
			usleep(5000);
		}
		myPktNum = getPktNum();
		reward = calcLaserReward(myPktNum, laserState);
		results[iter].action = laserState;
		results[iter].ir = packet.IRbyte;
		results[iter].reward = reward;
		printf("reward: %f\n", reward);

		nextState = getLaserFeatures(laserState, myPktNum);
		nextAction = epsilonGreedy(Q, nextState, epsilon);
		printf("nextAction: %u\n", nextAction);
		printf("before: %f, %f ", Q[state][0], Q[state][1]);
		Q[state][laserState]+= alpha * (reward + gamma*Q[nextState][nextAction] - Q[state][laserState]);
		printf("after: %f, %f\n", Q[state][0], Q[state][1]);
		state = nextState;
		laserState = nextAction;
		iter++;
	}

	writeResults("laserLearner.csv", results, testSize);

	return NULL;
}
/**
 *
 */
int main(int argc, char *argv[]) {

	if (strncmp(argv[1], "test", 4) == 0) {
		runTests();
	} else {

		signal(SIGINT, &endProgram);
		signal(SIGTSTP, &endProgram);
		signal(SIGQUIT, &endProgram);
		signal(SIGTERM, &endProgram);
		signal(SIGHUP, &endProgram);

		// consider splitting this framework stuff from the actor critic bit
		if (argc < 2) {
			fprintf(stderr,
					"Portname argument required -- something like /dev/tty.usbserial\n");
			return 0;
		}

		int model = CSP3_CREATE;
		int opt = getopt(argc, argv, "m:");
		while (-1 != opt) {
			switch (opt) {
			case 'm':
				if (strcmp(optarg, "500") == 0) {
					model = CSP3_ROOMBA_500;
				}
				break;
			}
		}

		srand(0);

		csp3Sensors sensors[] = { WHEEL_DROP, CLIFF_LEFT_SIGNAL,
				CLIFF_FRONT_LEFT_SIGNAL, CLIFF_FRONT_RIGHT_SIGNAL,
				CLIFF_RIGHT_SIGNAL, DISTANCE, IR };

		initCSP3(argv[1], argv[2], model, sensors,
				sizeof(sensors) / sizeof(int));
		usleep(20000); // wait for at least one packet to have arrived
		int tid = 0;
		int t_err = pthread_create(&tid, NULL, &laserLearner, NULL);
		if (t_err != 0) {
			fprintf(stderr, "\ncan't create thread: [%s]", strerror(t_err));
			exit(EXIT_FAILURE);
		}

		while(TRUE) {
			usleep(5000);
		}
		actorCritic();

		endCSP3();
	}

	return 0;
}
